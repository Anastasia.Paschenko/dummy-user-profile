import { TFunction } from "i18next";
import { Gender } from "./enums";

export const getGenderName = (t: TFunction<"translation", undefined, "translation">, gender: Gender): string => {
    if (gender === Gender.Male) return t("gender.male")
    if (gender === Gender.Female) return t("gender.female")
    return t("gender.other")
}

export const getMartialStatusName = (t: TFunction<"translation", undefined, "translation">, isMaried: boolean): string => {
    return isMaried ? t("maritalStatus.maried") : t("maritalStatus.notMaried")
}