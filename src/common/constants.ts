export const ROUTE = {
    HOME: "/",
    EDIT_PROFILE: "/edit",
};

export const LANGUAGE = {
    RU: "ru_RU",
    EN: "en_US",
};
