import { BrowserRouter } from "react-router-dom";
import { Router } from "./components/router/Router";
import "./i18n";

function App() {

  return (
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  );
}

export default App;
