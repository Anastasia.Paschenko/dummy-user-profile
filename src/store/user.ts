import { atom } from "jotai";
import { defaultUser, User } from "../models/User";

export const userAtom = atom<User>(defaultUser);
export const isSignupAtom = atom<boolean>((get) => !!get(userAtom).name)