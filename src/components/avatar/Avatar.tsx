import FaceIcon from "@mui/icons-material/Face";
import Face3Icon from "@mui/icons-material/Face3";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAlt";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Gender } from "../../common/enums";
import { useAtom } from "jotai";
import { userAtom, isSignupAtom } from "../../store/user";
import { Theme } from "@emotion/react";
import { SxProps } from "@mui/material";

const Avatar = ({ style }: { style?: SxProps<Theme> }): JSX.Element => {
    const [user] = useAtom(userAtom)
    const [isSignup] = useAtom(isSignupAtom)

    if (!isSignup) return <AccountCircleIcon sx={style} />
    if (user.gender === Gender.Male) return <FaceIcon sx={style} />
    if (user.gender === Gender.Female) return <Face3Icon sx={style} />
    return <SentimentSatisfiedAltIcon sx={style} />
}

export const HeaderAvatar = (): JSX.Element => (
    <Avatar style={{
        backgroundColor: "white",
        color: "black",
        fontSize: "40px",
        padding: "5px",
        borderRadius: "8px"
    }} />
)

export const ProfileAvatar = (): JSX.Element => <Avatar style={{ fontSize: "100px", marginTop: "100px" }} />