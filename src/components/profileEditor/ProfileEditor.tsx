import { Formik, Form } from "formik";
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, TextField } from "@mui/material";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import { Gender } from "../../common/enums";
import { defaultUser, User } from "../../models/User";
import "./styles.scss";
import { ProfileValidationSchema } from "./validation";
import { useTranslation } from "react-i18next";
import { useAtom } from "jotai";
import { isSignupAtom, userAtom } from "../../store/user";
import { useNavigate } from "react-router-dom";
import { ROUTE } from "../../common/constants";
import { getGenderName } from "../../common/helpers";

export const ProfileEditor = (): JSX.Element => {
    const { t } = useTranslation();
    const [user, setUser] = useAtom(userAtom)
    const [isSignup] = useAtom(isSignupAtom)
    const navigate = useNavigate();
    const onSubmit = (user: User) => {
        setUser(user)
        navigate(ROUTE.HOME)
    }

    return <div className="input-form">
        <h1>{isSignup ? t("titles.editProfile") : t("titles.signUp")}</h1>
        <Formik
            initialValues={isSignup ? user : defaultUser}
            onSubmit={onSubmit}
            validationSchema={ProfileValidationSchema}
            validateOnChange={false}
        >
            {formik => (
                <Form className="input-form-body">
                    <TextField
                        label={t("profile.name")}
                        fullWidth
                        id="name"
                        name="name"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.name && Boolean(formik.errors.name)}
                        helperText={formik.touched.name && formik.errors.name}
                    />
                    <TextField
                        label={t("profile.age")}
                        type="number"
                        fullWidth
                        id="age"
                        name="age"
                        value={formik.values.age}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.age && Boolean(formik.errors.age)}
                        helperText={formik.touched.age && formik.errors.age}
                    />
                    <TextField
                        label={t("profile.email")}
                        fullWidth
                        id="email"
                        name="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                    <FormControl style={{ alignSelf: "start" }}>
                        <FormLabel id="gender-radio-group-label">{t("profile.gender")}</FormLabel>
                        <RadioGroup
                            aria-labelledby="gender-radio-group-label"
                            id="gender"
                            name="gender"
                            value={formik.values.gender}
                            onChange={formik.handleChange}
                            style={{ paddingLeft: "30px" }}
                        >
                            <FormControlLabel value={Gender.Male} control={<Radio />} label={getGenderName(t, Gender.Male)} />
                            <FormControlLabel value={Gender.Female} control={<Radio />} label={getGenderName(t, Gender.Female)} />
                            <FormControlLabel value={Gender.Other} control={<Radio />} label={getGenderName(t, Gender.Other)} />
                        </RadioGroup>
                    </FormControl>
                    <FormControlLabel style={{ alignSelf: "start" }} control={
                        <Checkbox
                            id="isMaried"
                            name="isMaried"
                            value={formik.values.isMaried}
                            onChange={formik.handleChange}
                        />
                    } label={t("profile.isMaried")} />
                    <Button variant="contained" fullWidth type="submit">{isSignup ? t("buttons.save") : t("buttons.signUp")}</Button>
                </Form>
            )}
        </Formik>
    </div>
}