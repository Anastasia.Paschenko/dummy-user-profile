import * as Yup from "yup";

export const ProfileValidationSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, "Name is too Short!")
        .max(50, "Name is too Long!")
        .required("Required"),
    age: Yup.number()
        .min(14, "Age must be greater than 14")
        .max(100, "Age must be lower than 100")
        .required("Required"),
    email: Yup.string().email("Invalid email"),
});