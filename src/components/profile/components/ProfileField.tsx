import "./styles.scss";

export const ProfileField = ({ title, value }: { title: string, value: string }): JSX.Element => {
    return (
        <div className="profile-field">
            <div className="profile-field-title">{title}</div>
            <div className="profile-field-value">{value}</div>
        </div>
    )
}