import "./styles.scss";
import { useTranslation } from "react-i18next";
import { useAtom } from "jotai";
import { isSignupAtom, userAtom } from "../../store/user";
import { getGenderName, getMartialStatusName } from "../../common/helpers";
import { ProfileAvatar } from "../avatar/Avatar";
import { ProfileField } from "./components/ProfileField";

export const Profile = (): JSX.Element => {
    const { t } = useTranslation();
    const [user] = useAtom(userAtom)
    const [isSignup] = useAtom(isSignupAtom)

    if (!isSignup) return (
        <div className="profile">
            {t("messages.pleaseSignup")}
        </div>
    )

    return (
        <div className="profile">
            <ProfileAvatar />
            <ProfileField title={t("profile.name")} value={user.name} />
            <ProfileField title={t("profile.age")} value={`${user.age} ${t("years")}`} />
            <ProfileField title={t("profile.gender")} value={getGenderName(t, user.gender)} />
            <ProfileField title={t("profile.maritalStatus")} value={getMartialStatusName(t, user.isMaried)} />
        </div>
    )        
}