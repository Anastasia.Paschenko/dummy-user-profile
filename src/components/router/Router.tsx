import { Routes, Route, useLocation, Navigate } from "react-router-dom";
import { ROUTE } from "../../common/constants";
import { Header } from "../header/Header";
import { Profile } from "../profile/Profile";
import { ProfileEditor } from "../profileEditor/ProfileEditor";

export const Router = (): JSX.Element => {
    const location = useLocation();

    return (
        <>
            <Header />
            <Routes location={location}>
                <Route path={ROUTE.HOME} element={<Profile />} />
                <Route path={ROUTE.EDIT_PROFILE} element={<ProfileEditor />} />
                <Route path="*" element={<Navigate to={ROUTE.HOME} />} />
            </Routes>
        </>
    );
}

