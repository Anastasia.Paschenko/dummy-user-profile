import ruFlag from "../../../static/RU_flag.png";
import engFlag from "../../../static/UK_flag.png";
import { IconButton } from "@mui/material";
import { useTranslation } from "react-i18next";
import { LANGUAGE } from "../../../common/constants";

export const LanguageBtn = (): JSX.Element => {
    const { i18n: { changeLanguage, language } } = useTranslation();

    const switchLanguage = () => {
        changeLanguage(language === LANGUAGE.EN ? LANGUAGE.RU : LANGUAGE.EN);
    };

    return (
        <IconButton onClick={switchLanguage}>
            {language === LANGUAGE.EN
                ? <img src={engFlag} style={{ width: "40px" }} alt="Language"/>
                : <img src={ruFlag} style={{ width: "30px" }} alt="Язык" />
            }
        </IconButton>
    )
}