
import "./styles.scss";
import { useTranslation } from "react-i18next";
import { useAtom } from "jotai";
import { isSignupAtom, userAtom } from "../../store/user";
import { Button, IconButton } from "@mui/material";
import { Link } from "react-router-dom";
import { ROUTE } from "../../common/constants";
import { HeaderAvatar } from "../avatar/Avatar";
import { LanguageBtn } from "./components/LanguageBtn";

export const Header = (): JSX.Element => {
    const { t } = useTranslation();
    const [{ name }] = useAtom(userAtom)
    const [isSignup] = useAtom(isSignupAtom)

    return (
        <div className="main-header">
            <div className="main-header-welcome">
                <IconButton component={Link} to={ROUTE.HOME}>
                    <HeaderAvatar />
                </IconButton>
                <span className="main-header-welcome-title">{t("titles.welcome")}{isSignup ? name : t("guest")}</span>
            </div>
            <div>
                <Button
                    component={Link}
                    to={ROUTE.EDIT_PROFILE}
                    variant="outlined"
                    className="main-header-icon"
                    style={{ borderColor: "white", color: "white", height: "30px" }}
                >
                    {isSignup ? t("buttons.editProfile") : t("buttons.signUp")}
                </Button>
                <LanguageBtn />
            </div>
        </div>
    )
}