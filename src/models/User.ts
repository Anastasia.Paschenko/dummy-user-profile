import { Gender } from "../common/enums";

export interface User {
    name: string;
    age: string;
    email: string;
    isMaried: boolean;
    gender: Gender
}

export const defaultUser: User = {
    name: "",
    age: "",
    email: "",
    isMaried: false,
    gender: Gender.Male
}