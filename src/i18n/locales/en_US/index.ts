const translation = {
    guest: "Guest",
    years: "years",
    titles: {
        welcome: "Hi, ",
        editProfile: "Edit",
        signUp: "Sign Up",
    },
    buttons: {
        editProfile: "Edit",
        signUp: "Sign Up",
        save: "Save"
    },
    profile: {
        name: "Name",
        age: "Age",
        email: "Email",
        isMaried: "Maried",
        gender: "Gender",
        maritalStatus: "Marital status"

    },
    maritalStatus: {
        maried: "Maried",
        notMaried: "Not maried",
    },
    gender: {
        male: "Male",
        female: "Female",
        other: "Other"
    },
    messages: {
        pleaseSignup: "Please sign up to see your profile"
    }
};

export default translation;