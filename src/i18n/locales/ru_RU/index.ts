const translation = {
    guest: "Гость",
    years: "лет",
    titles: {
        welcome: "Привет, ",
        editProfile: "Редактировать",
        signUp: "Регистрация",
    },
    buttons: {
        editProfile: "Редактировать",
        signUp: "Регистрация",
        save: "Сохранить"
    },
    profile: {
        name: "Имя",
        age: "Возраст",
        email: "Эл. почта",
        isMaried: "Состою в браке",
        gender: "Пол",
        maritalStatus: "Семейное положение"

    },
    maritalStatus: {
        maried: "Женат/Замужем",
        notMaried: "Не женат/не замужем",
    },
    gender: {
        male: "Мужской",
        female: "Женский",
        other: "Другой"
    },
    messages: {
        pleaseSignup: "Зарегистрируйтесь для просмотра профиля"
    }
};

export default translation;