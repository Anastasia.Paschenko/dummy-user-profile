import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import ru from "./locales/ru_RU";
import en from "./locales/en_US";
import LanguageDetector from "i18next-browser-languagedetector";
import { LANGUAGE } from "../common/constants";

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        interpolation: {
            escapeValue: false,
        },
        lng: LANGUAGE.EN,
        resources: {
            [LANGUAGE.RU]: {
                translation: ru,
            },
            [LANGUAGE.EN]: {
                translation: en,
            },
        },
    });