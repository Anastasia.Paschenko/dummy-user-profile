# Dummy User Profile

### Functionality:
1) Fill user profile form
2) View user profile information (values ​​are reset after page refresh)
3) Change language (RU & EU)

### Libraries: 
1) Jotai
2) Formic
3) i18next